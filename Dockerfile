FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY D76Explorer.sln ./
COPY D76Explorer/*.csproj ./D76Explorer/
COPY D76ExplorerTest/*.csproj ./D76ExplorerTest/
RUN mkdir /app

RUN dotnet restore 
COPY . .
WORKDIR /src/D76Explorer
RUN dotnet build -c Release -o /app

FROM build AS publish
RUN dotnet publish -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .

ENV ASPNETCORE_URLS http://*:5000
EXPOSE 5000/tcp
ENTRYPOINT ["dotnet", "D76Explorer.dll"]
