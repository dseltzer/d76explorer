# D76Explorer

D76Explorer is a http gateway for making the CDC's D76 dataset easier to access.

## Data Access
Accessing the D76 endpoint still requires that you agree with the CDC's terms of use. This application makes data collection easier, but doesn't change your legal obligations under the CDC's terms of use.

## Technology
D76Explorer in dotnet core, running on kestral and packaged available as a docker image.

## Instructions
Run as an interactive http service using docker:
```
docker run -d --rm --name d76instance -p 5000:5000 registry.gitlab.com/dseltzer/d76explorer:latest
```

Test using curl
```
curl "http://localhost:5000/D76Explorer?saved_id=&dataset_code=D76&dataset_label=Underlying+Cause+of+Death%2C+1999-2018&dataset_vintage=2018&stage=request&O_javascript=on&M_1=D76.M1&M_2=D76.M2&M_3=D76.M3&O_aar=aar_none&B_1=D76.V9-level2&B_2=*None*&B_3=*None*&B_4=*None*&B_5=*None*&O_title=&O_oc-sect1-request=close&O_rate_per=100000&O_aar_pop=0000&VM_D76.M6_D76.V1_S=*All*&VM_D76.M6_D76.V7=*All*&VM_D76.M6_D76.V17=*All*&VM_D76.M6_D76.V8=*All*&VM_D76.M6_D76.V10=&O_location=D76.V9&finder-stage-D76.V9=codeset&O_V9_fmode=freg&V_D76.V9=&F_D76.V9=*All*&I_D76.V9=*All*+%28The+United+States%29%0D%0A&finder-stage-D76.V10=codeset&O_V10_fmode=freg&V_D76.V10=&F_D76.V10=*All*&I_D76.V10=*All*+%28The+United+States%29%0D%0A&finder-stage-D76.V27=codeset&O_V27_fmode=freg&V_D76.V27=&F_D76.V27=*All*&I_D76.V27=*All*+%28The+United+States%29%0D%0A&O_urban=D76.V19&V_D76.V19=*All*&V_D76.V11=*All*&O_age=D76.V5&V_D76.V5=*All*&V_D76.V51=*All*&V_D76.V52=*All*&V_D76.V6=00&V_D76.V7=*All*&V_D76.V17=*All*&V_D76.V8=*All*&finder-stage-D76.V1=codeset&O_V1_fmode=freg&V_D76.V1=&F_D76.V1=2017&F_D76.V1=2018&I_D76.V1=2017+%282017%29%0D%0A2018+%282018%29%0D%0A&V_D76.V24=*All*&V_D76.V20=*All*&V_D76.V21=*All*&O_ucd=D76.V22&finder-stage-D76.V2=codeset&O_V2_fmode=freg&V_D76.V2=&F_D76.V2=*All*&I_D76.V2=*All*+%28All+Causes+of+Death%29%0D%0A&V_D76.V4=*All*&V_D76.V12=*All*&V_D76.V22=3&V_D76.V23=GRINJ-006&finder-stage-D76.V25=codeset&O_V25_fmode=freg&V_D76.V25=&F_D76.V25=*All*&I_D76.V25=All+Causes+of+Death%0D%0A&O_change_action-Send-Export+Results=Export+Results&O_show_totals=true&O_show_zeros=true&O_show_suppressed=true&O_precision=1&O_timeout=600&action-Send=Send"
```

Shutdown the http service
```
docker stop d76instance
```
