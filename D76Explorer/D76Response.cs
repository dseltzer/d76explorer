﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace D76Explorer
{
    public class D76Response
    {
        public string Body { get; set; }
        public string Error { get; set; }
    }
}
