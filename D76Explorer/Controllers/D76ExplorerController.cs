﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;


namespace D76Explorer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class D76ExplorerController : ControllerBase
    {
        private IConfiguration _configuration;
        private string? _proxyServer = null;
        private readonly ILogger<D76ExplorerController> _logger;
        private static string AGREE_DATA = "stage=about&saved_id=&action-I+Agree=I+Agree";
        //private string REQUEST_DATA = "saved_id=&dataset_code=D76&dataset_label=Underlying+Cause+of+Death%2C+1999-2018&dataset_vintage=2018&stage=request&O_javascript=on&M_1=D76.M1&M_2=D76.M2&M_3=D76.M3&O_aar=aar_none&B_1=D76.V9-level2&B_2=*None*&B_3=*None*&B_4=*None*&B_5=*None*&O_title=&O_oc-sect1-request=close&O_rate_per=100000&O_aar_pop=0000&VM_D76.M6_D76.V1_S=*All*&VM_D76.M6_D76.V7=*All*&VM_D76.M6_D76.V17=*All*&VM_D76.M6_D76.V8=*All*&VM_D76.M6_D76.V10=&O_location=D76.V9&finder-stage-D76.V9=codeset&O_V9_fmode=freg&V_D76.V9=&F_D76.V9=*All*&I_D76.V9=*All*+%28The+United+States%29%0D%0A&finder-stage-D76.V10=codeset&O_V10_fmode=freg&V_D76.V10=&F_D76.V10=*All*&I_D76.V10=*All*+%28The+United+States%29%0D%0A&finder-stage-D76.V27=codeset&O_V27_fmode=freg&V_D76.V27=&F_D76.V27=*All*&I_D76.V27=*All*+%28The+United+States%29%0D%0A&O_urban=D76.V19&V_D76.V19=*All*&V_D76.V11=*All*&O_age=D76.V5&V_D76.V5=*All*&V_D76.V51=*All*&V_D76.V52=*All*&V_D76.V6=00&V_D76.V7=*All*&V_D76.V17=*All*&V_D76.V8=*All*&finder-stage-D76.V1=codeset&O_V1_fmode=freg&V_D76.V1=&F_D76.V1=2017&F_D76.V1=2018&I_D76.V1=2017+%282017%29%0D%0A2018+%282018%29%0D%0A&V_D76.V24=*All*&V_D76.V20=*All*&V_D76.V21=*All*&O_ucd=D76.V22&finder-stage-D76.V2=codeset&O_V2_fmode=freg&V_D76.V2=&F_D76.V2=*All*&I_D76.V2=*All*+%28All+Causes+of+Death%29%0D%0A&V_D76.V4=*All*&V_D76.V12=*All*&V_D76.V22=3&V_D76.V23=GRINJ-006&finder-stage-D76.V25=codeset&O_V25_fmode=freg&V_D76.V25=&F_D76.V25=*All*&I_D76.V25=All+Causes+of+Death%0D%0A&O_change_action-Send-Export+Results=Export+Results&O_show_totals=true&O_show_zeros=true&O_show_suppressed=true&O_precision=1&O_timeout=600&action-Send=Send";



        public D76ExplorerController(ILogger<D76ExplorerController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;

            _proxyServer = Environment.GetEnvironmentVariable("PROXY_SERVER");
        }

        [HttpGet]
        public D76Response Get()
        {
            var retval = new D76Response();
            var request_data = "";
            
            if(Request.QueryString.HasValue)
                request_data = Request.QueryString.ToString().Substring(1);            


            CookieContainer cookieJar = new CookieContainer();
            try
            {
                HttpWebRequest agreeRequest = (HttpWebRequest)WebRequest.Create("https://wonder.cdc.gov/controller/datarequest/D76");
                agreeRequest.ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
                agreeRequest.CookieContainer = cookieJar;
                agreeRequest.ContentType = "application/x-www-form-urlencoded";
                agreeRequest.Method = "POST";
                if (_proxyServer != null)
                    agreeRequest.Proxy = new WebProxy(new Uri(_proxyServer));

                using (var writer = new StreamWriter(agreeRequest.GetRequestStream(), System.Text.Encoding.ASCII))
                {
                    writer.Write(AGREE_DATA);
                }
                var agreeResponse = agreeRequest.GetResponse();
                var agreeBody = "";
                using (var reader = new StreamReader(agreeResponse.GetResponseStream()))
                {
                    agreeBody = reader.ReadToEnd();
                }
                var targetUrl = "https://wonder.cdc.gov" + Regex.Match(agreeBody, "action=\"(/controller/datarequest/D76;[^\"]+)\"").Groups[1].Value;


                HttpWebRequest dataRequest = (HttpWebRequest)WebRequest.Create(targetUrl);
                dataRequest.ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
                dataRequest.CookieContainer = cookieJar;
                dataRequest.ContentType = "application/x-www-form-urlencoded";
                dataRequest.Method = "POST";                

                if (_proxyServer != null)
                    dataRequest.Proxy = new WebProxy(new Uri(_proxyServer));

                using (var writer = new StreamWriter(dataRequest.GetRequestStream(), System.Text.Encoding.ASCII))
                {
                    writer.Write(request_data);
                }
                var dataResponse = dataRequest.GetResponse();
                var dataBody = "";
                using (var reader = new StreamReader(dataResponse.GetResponseStream()))
                {
                    dataBody = reader.ReadToEnd();
                }
                retval.Body = dataBody;
            }
            catch (System.Net.WebException wex)
            {
                _logger.LogError(wex, $"Error Retrieving D76: {wex.ToString()}");
                retval.Error = wex.ToString();
            }
            return retval;
        }
    }
}
